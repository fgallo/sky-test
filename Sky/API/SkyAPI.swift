//
//  SkyAPI.swift
//  Sky
//
//  Created by Fernando Gallo on 27/05/19.
//  Copyright © 2019 Fernando Gallo. All rights reserved.
//

import Foundation
import Moya

let skyBaseURL = URL(string: Constants.SkyAPI.endpoint)!

enum SkyAPI {
    case getMovies()
}

extension SkyAPI: TargetType {
    
    var baseURL: URL {
        return skyBaseURL
    }
    
    var path: String {
        switch self {
        case .getMovies:
            return "/Movies"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getMovies:
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getMovies:
            return stubbedResponse("Movies")
        }
    }
    
    var task: Task {
        switch self {
        case .getMovies:
            return .requestParameters(parameters: [:], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }

}


let SkyProvider = MoyaProvider<SkyAPI>(plugins:
    [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])

let SkyProviderTest = MoyaProvider<SkyAPI>(endpointClosure: { target in
    return Endpoint(url: url(target), sampleResponseClosure: {.networkResponse(200, target.sampleData)},
                    method: .get,
                    task: .requestPlain,
                    httpHeaderFields: nil)
},
                                           requestClosure: MoyaProvider<SkyAPI>.defaultRequestMapping,
                                           stubClosure: MoyaProvider.immediatelyStub,
                                           plugins: [NetworkLoggerPlugin(verbose: true,
                                                                         responseDataFormatter: JSONResponseDataFormatter)])

func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}

func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

func stubbedResponse(_ filename: String) -> Data! {
    @objc class TestClass: NSObject { }
    let bundle = Bundle(for: TestClass.self)
    let path = bundle.path(forResource: filename, ofType: "json")
    return (try? Data(contentsOf: URL(fileURLWithPath: path!)))
}
