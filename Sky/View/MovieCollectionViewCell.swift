//
//  MovieCollectionViewCell.swift
//  Sky
//
//  Created by Fernando Gallo on 27/05/19.
//  Copyright © 2019 Fernando Gallo. All rights reserved.
//

import UIKit
import Kingfisher

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    static let cellIdentifier = "MovieCell"
    
    var viewModel: MovieCellViewModel!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        movieImageView.layer.cornerRadius = 4.0
        movieImageView.clipsToBounds = true
    }
    
    func configure() {
        titleLabel.text = viewModel.title
        
        if let url = viewModel.coverURL {
            movieImageView.kf.indicatorType = .activity
            movieImageView.kf.setImage(
                with: url,
                placeholder: UIImage(named: "placeholder_movie"),
                options: [
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
        }
    }

}
