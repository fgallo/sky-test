//
//  MoviesViewController.swift
//  Sky
//
//  Created by Fernando Gallo on 27/05/19.
//  Copyright © 2019 Fernando Gallo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class MoviesViewController: UIViewController {
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let collectionViewPadding: CGFloat = 16.0
    private let disposeBag = DisposeBag()
    
    var viewModel: MoviesViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupCollectionView()
        getMovies()
    }
    
    
    // MARK: - Setup
    
    private func setupView() {
        title = "Cine SKY"
    }
    
    private func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: String(describing: MovieCollectionViewCell.self), bundle: nil),
                                forCellWithReuseIdentifier: MovieCollectionViewCell.cellIdentifier)
    }
    
    
    // MARK: - API
    
    private func getMovies() {
        activityIndicatorView.startAnimating()
        viewModel.fetchMovies()
            .drive(onNext: { _ in
                self.activityIndicatorView.stopAnimating()
                self.collectionView.reloadData()
            })
            .disposed(by: disposeBag)
    }
    
}


// MARK: - UICollectionViewDataSource and UICollectionViewDelegate

extension MoviesViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfMovies()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.cellIdentifier,
                                                      for: indexPath) as! MovieCollectionViewCell
        cell.viewModel = viewModel.viewModelForItemAt(indexPath: indexPath)
        cell.configure()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCell", for: indexPath)
        headerView.frame.size.height = 32.0
        return headerView
    }
    
}

extension MoviesViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movieDetailsViewController = UIStoryboard.main.movieDetailsViewController
        movieDetailsViewController.viewModel = viewModel.movieDetailsViewModelForItemAt(indexPath: indexPath)
        navigationController?.pushViewController(movieDetailsViewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0,
                            left: collectionViewPadding,
                            bottom: collectionViewPadding,
                            right: collectionViewPadding)
    }
    
}

extension MoviesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.frame.size.width - (collectionViewPadding *  3)
        let width = collectionViewWidth / 2
        let height = width * 1.8
        
        return CGSize(width: width, height: height)
    }
    
}
