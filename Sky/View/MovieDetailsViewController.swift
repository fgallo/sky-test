//
//  MovieDetailsViewController.swift
//  Sky
//
//  Created by Fernando Gallo on 27/05/19.
//  Copyright © 2019 Fernando Gallo. All rights reserved.
//

import UIKit
import Kingfisher

class MovieDetailsViewController: UIViewController {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearAndDurationLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    var viewModel: MovieDetailsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupCoverImageView()
        setupLabels()
    }
    
    
    // MARK: - Setup
    
    private func setupView() {
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0.0, y: coverImageView.frame.maxY - 32, width: view.bounds.width, height: 32)
        gradient.colors = [UIColor.SkyColor.DefaultBackground.withAlphaComponent(0.0).cgColor,
                           UIColor.SkyColor.DefaultBackground.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        self.view.layer.insertSublayer(gradient, above: coverImageView.layer)
    }
    
    private func setupCoverImageView() {
        if let url = viewModel.movieCoverURL() {
            coverImageView.kf.indicatorType = .activity
            coverImageView.kf.setImage(
                with: url,
                options: [
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
        }
    }
    
    private func setupLabels() {
        titleLabel.text = viewModel.movieTitle()
        yearAndDurationLabel.text = viewModel.movieYear() + " - " + viewModel.movieDuration()
        overviewLabel.text = viewModel.movieOverview()
    }    
    
}
