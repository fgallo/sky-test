//
//  Transformations.swift
//  Sky
//
//  Created by Fernando Gallo on 27/05/19.
//  Copyright © 2019 Fernando Gallo. All rights reserved.
//

import Foundation
import ObjectMapper

class JSONStringToURLTransform: TransformType {
    typealias Object = URL
    typealias JSON = String
    
    func transformFromJSON(_ value: Any?) -> URL? {
        if let strValue = value as? String {
            return URL(string: strValue.replacingOccurrences(of: "w640", with: "w1280"))
        }
        return nil
    }
    
    func transformToJSON(_ value: URL?) -> String? {
        if let urlValue = value {
            return urlValue.absoluteString
        }
        return nil
    }
    
}
