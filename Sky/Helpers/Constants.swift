//
//  Constants.swift
//  Sky
//
//  Created by Fernando Gallo on 27/05/19.
//  Copyright © 2019 Fernando Gallo. All rights reserved.
//

import Foundation

struct Constants {
    
    struct SkyAPI {
        static let endpoint = "https://sky-exercise.herokuapp.com/api"
    }
    
}
