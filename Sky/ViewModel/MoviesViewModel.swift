//
//  MoviesViewModel.swift
//  Sky
//
//  Created by Fernando Gallo on 27/05/19.
//  Copyright © 2019 Fernando Gallo. All rights reserved.
//

import Foundation
import Moya
import RxCocoa
import RxSwift
import Moya_ObjectMapper

class MoviesViewModel {
    
    private let skyProvider: MoyaProvider<SkyAPI>
    private var movies: [Movie]
    
    init(skyProvider: MoyaProvider<SkyAPI>) {
        self.skyProvider = skyProvider
        self.movies = []
    }
    
    
    // MARK: - API
    
    func fetchMovies() -> Driver<[Movie]> {
        return skyProvider.rx
            .request(SkyAPI.getMovies())
            .mapArray(Movie.self)
            .do(onSuccess: { movies in
                self.movies = movies
            })
            .asDriver(onErrorDriveWith: Driver<[Movie]>.empty())
    }
    
    
    // MARK: - Movies
    
    func numberOfMovies() -> Int {
        return movies.count
    }
    
    func movieForItemAt(indexPath: IndexPath) -> Movie {
        return movies[indexPath.row]
    }
    
    func viewModelForItemAt(indexPath: IndexPath) -> MovieCellViewModel {
        let movie = movies[indexPath.row]
        return MovieCellViewModel(movie: movie)
    }
    
    func movieDetailsViewModelForItemAt(indexPath: IndexPath) -> MovieDetailsViewModel {
        let movie = movies[indexPath.row]
        return MovieDetailsViewModel(movie: movie)
    }
    
}
