//
//  MovieCellViewModel.swift
//  Sky
//
//  Created by Fernando Gallo on 27/05/19.
//  Copyright © 2019 Fernando Gallo. All rights reserved.
//

import Foundation

class MovieCellViewModel {
    
    let title: String
    let coverURL: URL?
    
    init(movie: Movie) {
        self.title = movie.title ?? ""
        self.coverURL = movie.coverURL
    }
    
}
