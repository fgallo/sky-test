//
//  MovieDetailsViewModel.swift
//  Sky
//
//  Created by Fernando Gallo on 27/05/19.
//  Copyright © 2019 Fernando Gallo. All rights reserved.
//

import Foundation

class MovieDetailsViewModel {
    
    private let movie: Movie
    
    init(movie: Movie) {
        self.movie = movie
    }
    
    func movieTitle() -> String {
        return movie.title ?? ""
    }
    
    func movieYear() -> String {
        return movie.releaseYear ?? ""
    }
    
    func movieDuration() -> String {
        return movie.duration ?? ""
    }
    
    func movieOverview() -> String {
        return movie.overview ?? ""
    }
    
    func movieCoverURL() -> URL? {
        if let covers = movie.backdrops, covers.count > 0 {
            return URL(string: covers[0])
        }
        
        return nil
    }
    
}
