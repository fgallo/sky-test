//
//  Movie.swift
//  Sky
//
//  Created by Fernando Gallo on 27/05/19.
//  Copyright © 2019 Fernando Gallo. All rights reserved.
//

import Foundation
import ObjectMapper

struct Movie: Mappable {
    
    var id: String?
    var title: String?
    var coverURL: URL?
    var releaseYear: String?
    var duration: String?
    var overview: String?
    var backdrops: [String]?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        coverURL <- (map["cover_url"], JSONStringToURLTransform())
        releaseYear <- map["release_year"]
        duration <- map["duration"]
        overview <- map["overview"]
        backdrops <- map["backdrops_url"]
    }
    
}
