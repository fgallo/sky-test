//
//  MoviesViewModelTests.swift
//  SkyTests
//
//  Created by Fernando Gallo on 28/05/19.
//  Copyright © 2019 Fernando Gallo. All rights reserved.
//

import XCTest
@testable import Sky
@testable import RxSwift
@testable import RxCocoa

class MoviesViewModelTests: XCTestCase {
    
    var disposeBag: DisposeBag!
    var viewModel: MoviesViewModel!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        viewModel = MoviesViewModel(skyProvider: SkyProviderTest)
    }
    
    override func tearDown() {
        disposeBag = nil
        viewModel = nil
        super.tearDown()
    }
    
    func testFetchMoviesParse() {
        let promise = expectation(description: "driveOnNext called")
        
        viewModel.fetchMovies()
            .drive(onNext: { movies in
                let movie = movies[0]
                
                XCTAssertEqual(movie.title, "Doutor Estranho", "Error on parsing movie title.")
                XCTAssertEqual(movie.coverURL, URL(string: "https://image.tmdb.org/t/p/w1280/dsAQmTOCyMDgmiPp9J4aZTvvOJP.jpg"), "Error on parsing movie cover URL.")
                XCTAssertEqual(movie.releaseYear, "2017", "Error on parsing movie release year.")
                XCTAssertEqual(movie.duration, "1h 55m", "Error on parsing movie duration.")
                XCTAssertEqual(movie.overview, "Stephen Strange (Benedict Cumberbatch) leva uma vida bem sucedida como neurocirurgião. Sua vida muda completamente quando sofre um acidente de carro e fica com as mãos debilitadas. Devido a falhas da medicina tradicional, ele parte para um lugar inesperado em busca de cura e esperança, um misterioso enclave chamado Kamar-Taj, localizado em Katmandu. Lá descobre que o local não é apenas um centro medicinal, mas também a linha de frente contra forças malignas místicas que desejam destruir nossa realidade. Ele passa a treinar e adquire poderes mágicos, mas precisa decidir se vai voltar para sua vida comum ou defender o mundo.",
                               "Error on parsing movie overview.")
                
                promise.fulfill()
            })
            .disposed(by: disposeBag)
        
        waitForExpectations(timeout: 3.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testNumberOfMoviesWhenInit() {
        let count = viewModel.numberOfMovies()
        XCTAssert(count == 0)
    }
    
    func testNumberOfMoviesAfterFetch() {
        let promise = expectation(description: "driveOnNext called")
        
        viewModel.fetchMovies()
            .drive(onNext: { _ in
                promise.fulfill()
            })
            .disposed(by: disposeBag)
        
        waitForExpectations(timeout: 3.0) { error in
            let count = self.viewModel.numberOfMovies()
            XCTAssert(count > 0)
        }
    }
    
    func testMovieForItem() {
        let promise = expectation(description: "driveOnNext called")
        var movie: Movie?
        
        viewModel.fetchMovies()
            .drive(onNext: { movies in
                movie = movies[0]
                promise.fulfill()
            })
            .disposed(by: disposeBag)
        
        waitForExpectations(timeout: 3.0) { error in
            let movieForItem = self.viewModel.movieForItemAt(indexPath: IndexPath(item: 0, section: 0))
            XCTAssert(movieForItem.id == movie?.id)
        }
    }
    
    func testViewModelForItem() {
        let promise = expectation(description: "driveOnNext called")
        var movie: Movie?
        
        viewModel.fetchMovies()
            .drive(onNext: { movies in
                movie = movies[0]
                promise.fulfill()
            })
            .disposed(by: disposeBag)
        
        waitForExpectations(timeout: 3.0) { error in
            let viewModelForItem = self.viewModel.viewModelForItemAt(indexPath: IndexPath(item: 0, section: 0))
            XCTAssert(viewModelForItem.title == movie?.title)
            XCTAssert(viewModelForItem.coverURL == movie?.coverURL)
        }
    }

}
